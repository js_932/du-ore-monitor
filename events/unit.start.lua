------------------------------------------
-- Change the code below at your own risk!
------------------------------------------
--TODO: implement raw ore tracking
PURES_CONTAINERS = getAllPureContainers()

-- pure container names and kg/l table.
PURES = {}
-- tier 1
PURES["aluminium"] = {volume = 0, percent = 0, mass = 2.7}
PURES["carbon"] = {volume = 0, percent = 0, mass = 2.27}
PURES["iron"] = {volume = 0, percent = 0, mass = 7.85}
PURES["silicon"] = {volume = 0, percent = 0, mass = 2.33}
-- tier 2
PURES["calcium"] = {volume = 0, percent = 0, mass = 1.55}
PURES["chromium"] = {volume = 0, percent = 0, mass = 7.19}
PURES["copper"] = {volume = 0, percent = 0, mass = 8.96}
PURES["sodium"] = {volume = 0, percent = 0, mass = 0.97}
-- tier 3
PURES["lithium"] = {volume = 0, percent = 0, mass = 0.53}
PURES["nickel"] = {volume = 0, percent = 0, mass = 8.91}
PURES["silver"] = {volume = 0, percent = 0, mass = 10.49}
PURES["sulfur"] = {volume = 0, percent = 0, mass = 1.82}
-- tier 4
PURES["cobalt"] = {volume = 0, percent = 0, mass = 8.9}
PURES["fluorine"] = {volume = 0, percent = 0, mass = 1.7}
PURES["gold"] = {volume = 0, percent = 0, mass = 19.3}
PURES["scandium"] = {volume = 0, percent = 0, mass = 2.98}
-- tier 5
PURES["manganese"] = {volume = 0, percent = 0, mass = 7.21}
PURES["niobium"] = {volume = 0, percent = 0, mass = 8.57}
PURES["titanium"] = {volume = 0, percent = 0, mass = 4.51}
PURES["vanadium"] = {volume = 0, percent = 0, mass = 6}

-- html for screens
screen_css =
    [[body{background-color: #0c0032; color: whitesmoke; padding: .2em;}table{width: 100%%; height: 80%%; border: .5em solid #282828; border-top: .25em; border-radius: 1em;}table:first-of-type{border-top: .5em solid #282828;}th{text-align: left; width: 33%%; font-size: 4em; background-color: #0c0032;}td{font-size: 3.5em;}tr:nth-of-type(even){background-color: #3500d3;}tr:nth-of-type(odd){background-color: #240090;}]]

-- TODO: clean up this function to avoid repetition
-- replaces variable placeholders with correct variables
function insertVariablesIntoHTML()
    -- html to send to the screen
    local ore_table_html = {
        t1 = [[<html><head><style></style></head><body><table><tr><th>Tier 1</th><th>Volume</th><th>Percent</th></tr><tr> <td>Aluminium</td><td>param_aluminium_vol kl</td><td>param_aluminium_percent%</td></tr><tr> <td>Carbon</td><td>param_carbon_vol kl</td><td>param_carbon_percent%</td></tr><tr> <td>Iron</td><td>param_iron_vol kl</td><td>param_iron_percent%</td></tr><tr> <td>Silicon</td><td>param_silicon_vol kl</td><td>param_silicon_percent%</td></tr></table><p class="alert">Alert: param_alert</p></body></html>]],
        t2 = [[<html><head><style></style></head><body><table><tr><th>Tier 2</th><th>Volume</th><th>Percent</th></tr><tr> <td>Calcium</td><td>param_calcium_vol kl</td><td>param_calcium_percent%</td></tr><tr> <td>Chromium</td><td>param_chromium_vol kl</td><td>param_chromium_percent%</td></tr><tr> <td>Copper</td><td>param_copper_vol kl</td><td>param_copper_percent%</td></tr><tr> <td>Sodium</td><td>param_sodium_vol kl</td><td>param_sodium_percent%</td></tr></table><p class="alert">Alert: param_alert</p></body></html>]],
        t3 = [[<html><head><style></style></head><body><table><tr><th>Tier 3</th><th>Volume</th><th>Percent</th></tr><tr> <td>Lithium</td><td>param_lithium_vol kl</td><td>param_lithium_percent%</td></tr><tr> <td>Nickel</td><td>param_nickel_vol kl</td><td>param_nickel_percent%</td></tr><tr> <td>Silver</td><td>param_silver_vol kl</td><td>param_silver_percent%</td></tr><tr> <td>Sulfur</td><td>param_sulfur_vol kl</td><td>param_sulfur_percent%</td></tr></table><p class="alert">Alert: param_alert</p></body></html>]],
        t4 = [[<html><head><style></style></head><body><table><tr><th>Tier 4</th><th>Volume</th><th>Percent</th></tr><tr> <td>Cobalt</td><td>param_cobalt_vol kl</td><td>param_cobalt_percent%</td></tr><tr> <td>Fluorine </td><td>param_fluorine_vol kl</td><td>param_fluorine_percent%</td></tr><tr> <td>Gold</td><td>param_gold_vol kl</td><td>param_gold_percent%</td></tr><tr> <td>Scandium</td><td>param_scandium_vol kl</td><td>param_scandium_percent%</td></tr></table><p class="alert">Alert: param_alert</p></body></html>]],
        t5 = [[<html><head><style></style></head><body><table><tr><th>Tier 5</th><th>Volume</th><th>Percent</th></tr><tr> <td>Manganese</td><td>param_manganese_vol kl</td><td>param_manganese_percent%</td></tr><tr> <td>Niobium </td><td>param_niobium_vol kl</td><td>param_niobium_percent%</td></tr><tr> <td>Titanium</td><td>param_titanium_vol kl</td><td>param_titanium_percent%</td></tr><tr> <td>Vanadium</td><td>param_vanadium_vol kl</td><td>param_vanadium_percent%</td></tr></table><p class="alert">Alert: param_alert</p></body></html>]]
    }
    -- aluminium string replace
    ore_table_html.t1 = ore_table_html.t1:gsub("param_aluminium_vol", string.format("%.2f", PURES["aluminium"].volume))
    ore_table_html.t1 =
        ore_table_html.t1:gsub("param_aluminium_percent", string.format("%.2f", PURES["aluminium"].percent))
    -- carbon string replace
    ore_table_html.t1 = ore_table_html.t1:gsub("param_carbon_vol", string.format("%.2f", PURES["carbon"].volume))
    ore_table_html.t1 = ore_table_html.t1:gsub("param_carbon_percent", string.format("%.2f", PURES["carbon"].percent))
    -- iron string replace
    ore_table_html.t1 = ore_table_html.t1:gsub("param_iron_vol", string.format("%.2f", PURES["iron"].volume))
    ore_table_html.t1 = ore_table_html.t1:gsub("param_iron_percent", string.format("%.2f", PURES["iron"].percent))
    -- silicon string replace
    ore_table_html.t1 = ore_table_html.t1:gsub("param_silicon_vol", string.format("%.2f", PURES["silicon"].volume))
    ore_table_html.t1 = ore_table_html.t1:gsub("param_silicon_percent", string.format("%.2f", PURES["silicon"].percent))
    -- calcium string replace
    ore_table_html.t2 = ore_table_html.t2:gsub("param_calcium_vol", string.format("%.2f", PURES["calcium"].volume))
    ore_table_html.t2 = ore_table_html.t2:gsub("param_calcium_percent", string.format("%.2f", PURES["calcium"].percent))
    -- chromium string replace
    ore_table_html.t2 = ore_table_html.t2:gsub("param_chromium_vol", string.format("%.2f", PURES["chromium"].volume))
    ore_table_html.t2 =
        ore_table_html.t2:gsub("param_chromium_percent", string.format("%.2f", PURES["chromium"].percent))
    -- copper string replace
    ore_table_html.t2 = ore_table_html.t2:gsub("param_copper_vol", string.format("%.2f", PURES["copper"].volume))
    ore_table_html.t2 = ore_table_html.t2:gsub("param_copper_percent", string.format("%.2f", PURES["copper"].percent))
    -- sodium string replace
    ore_table_html.t2 = ore_table_html.t2:gsub("param_sodium_vol", string.format("%.2f", PURES["sodium"].volume))
    ore_table_html.t2 = ore_table_html.t2:gsub("param_sodium_percent", string.format("%.2f", PURES["sodium"].percent))
    -- lithium string replace
    ore_table_html.t3 = ore_table_html.t3:gsub("param_lithium_vol", string.format("%.2f", PURES["lithium"].volume))
    ore_table_html.t3 = ore_table_html.t3:gsub("param_lithium_percent", string.format("%.2f", PURES["lithium"].percent))
    -- nickel string replace
    ore_table_html.t3 = ore_table_html.t3:gsub("param_nickel_vol", string.format("%.2f", PURES["nickel"].volume))
    ore_table_html.t3 = ore_table_html.t3:gsub("param_nickel_percent", string.format("%.2f", PURES["nickel"].percent))
    -- silver string replace
    ore_table_html.t3 = ore_table_html.t3:gsub("param_silver_vol", string.format("%.2f", PURES["silver"].volume))
    ore_table_html.t3 = ore_table_html.t3:gsub("param_silver_percent", string.format("%.2f", PURES["silver"].percent))
    -- sulfur string replace
    ore_table_html.t3 = ore_table_html.t3:gsub("param_sulfur_vol", string.format("%.2f", PURES["sulfur"].volume))
    ore_table_html.t3 = ore_table_html.t3:gsub("param_sulfur_percent", string.format("%.2f", PURES["sulfur"].percent))
    -- cobalt string replace
    ore_table_html.t4 = ore_table_html.t4:gsub("param_cobalt_vol", string.format("%.2f", PURES["cobalt"].volume))
    ore_table_html.t4 = ore_table_html.t4:gsub("param_cobalt_percent", string.format("%.2f", PURES["cobalt"].percent))
    -- fluorine string replace
    ore_table_html.t4 = ore_table_html.t4:gsub("param_fluorine_vol", string.format("%.2f", PURES["fluorine"].volume))
    ore_table_html.t4 =
        ore_table_html.t4:gsub("param_fluorine_percent", string.format("%.2f", PURES["fluorine"].percent))
    -- gold string replace
    ore_table_html.t4 = ore_table_html.t4:gsub("param_gold_vol", string.format("%.2f", PURES["gold"].volume))
    ore_table_html.t4 = ore_table_html.t4:gsub("param_gold_percent", string.format("%.2f", PURES["gold"].percent))
    -- scandium string replace
    ore_table_html.t4 = ore_table_html.t4:gsub("param_scandium_vol", string.format("%.2f", PURES["scandium"].volume))
    ore_table_html.t4 =
        ore_table_html.t4:gsub("param_scandium_percent", string.format("%.2f", PURES["scandium"].percent))
    -- manganese string replace
    ore_table_html.t5 = ore_table_html.t5:gsub("param_manganese_vol", string.format("%.2f", PURES["manganese"].volume))
    ore_table_html.t5 =
        ore_table_html.t5:gsub("param_manganese_percent", string.format("%.2f", PURES["manganese"].percent))
    -- niobium string replace
    ore_table_html.t5 = ore_table_html.t5:gsub("param_niobium_vol", string.format("%.2f", PURES["niobium"].volume))
    ore_table_html.t5 = ore_table_html.t5:gsub("param_niobium_percent", string.format("%.2f", PURES["niobium"].percent))
    -- titanium string replace
    ore_table_html.t5 = ore_table_html.t5:gsub("param_titanium_vol", string.format("%.2f", PURES["titanium"].volume))
    ore_table_html.t5 =
        ore_table_html.t5:gsub("param_titanium_percent", string.format("%.2f", PURES["titanium"].percent))
    -- vanadium string replace
    ore_table_html.t5 = ore_table_html.t5:gsub("param_vanadium_vol", string.format("%.2f", PURES["vanadium"].volume))
    ore_table_html.t5 =
        ore_table_html.t5:gsub("param_vanadium_percent", string.format("%.2f", PURES["vanadium"].percent))

    applyCSS(ore_table_html)
    setScreenHtml(ore_table_html)
end

-- calculate the fill percent and volume of ore in container
function calculateVolumeAndFillLevel(_id, container_mass, capacity, name)
    local volume
    local percent = 0

    volume = (core.getElementMassById(_id) - container_mass)
    volume = ((volume - OPTIMISATION_MODIFIER) / PURES[name].mass) / 1000

    if volume > 0 then
        percent = (volume / (capacity / 1000)) * 100
    end

    return volume, percent
end

-- updates the volume and fill percent of the PURES
function updateOreValues()
    for k, v in pairs(PURES_CONTAINERS) do
        if PURES[v.name] then
            PURES[v.name].volume, PURES[v.name].percent =
                calculateVolumeAndFillLevel(v.id, v.mass, v.max_volume, v.name)
        end
    end
    insertVariablesIntoHTML()
end

-- sets the html of the respective screens
function setScreenHtml(screen_content)
    t1_screen.setHTML(screen_content.t1)
    t2_screen.setHTML(screen_content.t2)
    t3_screen.setHTML(screen_content.t3)
    t4_screen.setHTML(screen_content.t4)
    t5_screen.setHTML(screen_content.t5)
end

-- add css to all screen html
function applyCSS(screen_html)
    screen_html.t1 = screen_html.t1:gsub("<style></style>", "<style>" .. screen_css .. "</style>")
    screen_html.t2 = screen_html.t2:gsub("<style></style>", "<style>" .. screen_css .. "</style>")
    screen_html.t3 = screen_html.t3:gsub("<style></style>", "<style>" .. screen_css .. "</style>")
    screen_html.t4 = screen_html.t4:gsub("<style></style>", "<style>" .. screen_css .. "</style>")
    screen_html.t5 = screen_html.t5:gsub("<style></style>", "<style>" .. screen_css .. "</style>")
end

-- TODO: Add alerts to the screens to allow quick checking of the ores that are
-- lower than a specified amount set by the user.

-- set the tick speed and name
unit.setTimer("ore_update", 10)
updateOreValues()

-- debug if true
if DEBUG then
    system.print("Containers: " .. #PURES_CONTAINERS)
    system.print(PURES["aluminium"].volume)
end
