------ MAKE SURE TO SET USER VARIABLES BELOW ------
-- container talents, required to detemine volumes and weights of ore
local container_proficiency = 0 -- export: container proficiency modifier.
local container_optimisation = 0 -- export: container optimisation modifier.
-- other user variables
local show_help = false -- export: set to true for help in setting up the system.

----------------------------------------
-- Change the code below at your own risk!
------------------------------------------
DEBUG = false
-- containers must be named in lowercase with a prefix of "pure_" e.g pure_aluminium

-- container talent modifiers, do not change
PROFICIENCY_MODIFIER = (container_proficiency * 10) / 100
OPTIMISATION_MODIFIER = (container_optimisation * 5) / 100

-- stats of the various containers
--container l stats
local base_hp_l = 17316
local mass_l = 14842.7
local base_vol_l = 128000
--container m stats
local base_hp_m = 7997
local mass_m = 7421.35
local base_vol_m = 64000
--container s stats
local base_hp_s = 999
local mass_s = 1281.31
local base_vol_s = 8000
--container xs stats
local base_hp_xs = 124
local mass_xs = 229.09
local base_vol_xs = 1000

-- returns the containers max volume and max_mass after modifiers
function getContainerSize(_id)
    local max_hp = core.getElementMaxHitPointsById(_id)

    if max_hp >= base_hp_l then
        return base_vol_l, mass_l
    elseif max_hp >= base_hp_m then
        return base_vol_m, mass_m
    elseif max_hp >= base_hp_s then
        return base_vol_s, mass_s
    else
        return base_vol_xs, mass_xs
    end

    if DEBUG then
        system.print("in getContainerSize(): max_hp " .. max_hp .. "max_volume = " .. max_volume .. " mass " .. mass)
    end
    return max_volume, mass
end

-- creates a container table for a pure container
function newPureContainer(_id)
    local _max_volume, _mass = getContainerSize(_id)
    local element = {
        id = _id,
        name = core.getElementNameById(_id):gsub("pure_", ""),
        max_volume = _max_volume + (_max_volume * PROFICIENCY_MODIFIER),
        mass = _mass
    }
    if DEBUG then
        system.print(
            "in newPureContainer(): id = " ..
                element.id ..
                    " name = " .. element.name .. " max_volume = " .. element.max_volume .. " mass = " .. element.mass
        )
    end
    return element
end

-- gets all the pure ore containers in the core and returns them in a table
function getAllPureContainers()
    local result = {}
    for cur, id in pairs(core.getElementIdList()) do
        if core.getElementClass then
            if
                string.match(core.getElementTypeById(id), "ontainer") and
                    string.match(core.getElementNameById(id), "pure_")
             then
                table.insert(result, newPureContainer(id))
            end
        end
    end
    return result
end
